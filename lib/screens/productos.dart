//pantalla para los productos
import 'dart:convert';

import 'package:coperativa_app_2/models/product.dart';
import 'package:coperativa_app_2/widgets/new_product.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

final _firebase = FirebaseAuth.instance;

class Productoslista extends StatefulWidget {
  const Productoslista({super.key});

  @override
  State<Productoslista> createState() => _ProductoslistaState();
}

class _ProductoslistaState extends State<Productoslista> {
  List<ProductItem> _productItems = [];
  var isLoading = true;
  //String? _error;

  @override
  void initState() {
    super.initState();
    _loadIteams();
  }

  _loadIteams() async {
    final url = Uri.https('coperativa-flutter-default-rtdb.firebaseio.com',
        'productos-list.json');
    final response = await http.get(url);
    final Map<String, dynamic> listData = json.decode(response.body);

    final List<ProductItem> loadedItems = [];

    for (final item in listData.entries) {
      loadedItems.add(
        ProductItem(
          id: item.key,
          nombre: item.value['nombre'],
          stock: item.value['stock'],
          precio: item.value['precio'],
          color: item.value['color'],
        ),
      );
    }
    setState(() {
      _productItems = loadedItems;
      isLoading = false;
    });
  }

  void _addItem() async {
    final newItem = await Navigator.of(context).push<ProductItem>(
      MaterialPageRoute(
        builder: (context) => const NewProducto(),
      ),
    );
    if (newItem == null) {
      return;
    }
    setState(() {
      _productItems.add(newItem);
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget content = const Center(
      child: Text('Sin informaciòn'),
    );
    if (isLoading) {
      content = const Center(
        child: CircularProgressIndicator(),
      );
    }
    if (_productItems.isNotEmpty) {
      content = ListView.builder(
        itemCount: _productItems.length,
        itemBuilder: (ctx, index) => Dismissible(
          onDismissed: (direction) {
            // _removeItem(_groceryItems[index]);
          },
          key: ValueKey(_productItems[index].id),
          child: ListTile(
            title: Text(_productItems[index].nombre),
            leading: Container(
              width: 24,
              height: 24,
              color: Colors.white,
            ),
            trailing: Text(
              _productItems[index].stock.toString(),
            ),
          ),
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lista de productos'),
        actions: [
          IconButton(
            onPressed: _addItem,
            icon: const Icon(Icons.add),
            color: Theme.of(context).colorScheme.primary,
          ),
          IconButton(
            onPressed: () {
              FirebaseAuth.instance.signOut();
            },
            icon: Icon(
              Icons.exit_to_app,
              color: Theme.of(context).colorScheme.primary,
            ),
          )
        ],
      ),
      body: content,
    );
  }
}
