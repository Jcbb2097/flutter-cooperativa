//pantalla para los nuevos usuarios

import 'dart:convert';

import 'package:coperativa_app_2/models/users.dart';
import 'package:coperativa_app_2/widgets/new_user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

final _firebase = FirebaseAuth.instance;

class UsuariosLista extends StatefulWidget {
  const UsuariosLista({super.key});
  @override
  State<UsuariosLista> createState() => _UsuariosListaState();
}

class _UsuariosListaState extends State<UsuariosLista> {
  List<UsuarioItem> _usersItems = [];
  var isLoading = true;
  //String? _error;

  @override
  void initState() {
    super.initState();
    _loadIteams();
  }

  _loadIteams() async {
    final url = Uri.https(
        'coperativa-flutter-default-rtdb.firebaseio.com', 'usarios-list.json');

    final response = await http.get(url);
    final Map<String, dynamic> listData = json.decode(response.body);

    final List<UsuarioItem> loadedItems = [];

    for (final item in listData.entries) {
      loadedItems.add(
        UsuarioItem(
          id: item.key,
          correo: item.value['email'],
          usuario: item.value['usuario'],
          paswoord: item.value['contraseña'],
          perfil: item.value['perfil'],
        ),
      );
    }
    setState(() {
      _usersItems = loadedItems;
      isLoading = false;
    });
  }

  void _addItem() async {
    final newItem = await Navigator.of(context).push<UsuarioItem>(
      MaterialPageRoute(
        builder: (context) => const NewUser(),
      ),
    );
    if (newItem == null) {
      return;
    }
    setState(() {
      _usersItems.add(newItem);
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget content = const Center(
      child: Text('Sin informaciòn'),
    );
    if (isLoading) {
      content = const Center(
        child: CircularProgressIndicator(),
      );
    }
    if (_usersItems.isNotEmpty) {
      content = ListView.builder(
        itemCount: _usersItems.length,
        itemBuilder: (ctx, index) => Dismissible(
          onDismissed: (direction) {
            // _removeItem(_groceryItems[index]);
          },
          key: ValueKey(_usersItems[index].id),
          child: ListTile(
            title: Text(_usersItems[index].usuario),
            leading: Container(
              width: 24,
              height: 24,
              color: Colors.white,
            ),
            trailing: Text(
              _usersItems[index].perfil.toString(),
            ),
          ),
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lista de usuarios'),
        actions: [
          IconButton(
            onPressed: _addItem,
            icon: const Icon(Icons.add),
            color: Theme.of(context).colorScheme.primary,
          ),
          IconButton(
            onPressed: () {
              FirebaseAuth.instance.signOut();
            },
            icon: Icon(
              Icons.exit_to_app,
              color: Theme.of(context).colorScheme.primary,
            ),
          )
        ],
      ),
      body: content,
    );
  }
}
