//pantalla para los usuarios
import 'dart:convert';

import 'package:coperativa_app_2/data/grados.dart';
import 'package:coperativa_app_2/models/alumno.dart';
import 'package:coperativa_app_2/widgets/new_alumn.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

final _firebase = FirebaseAuth.instance;

class ListaAlumnos extends StatefulWidget {
  const ListaAlumnos({super.key});
  @override
  State<ListaAlumnos> createState() => _ListaAlumnosState();
}

class _ListaAlumnosState extends State<ListaAlumnos> {
  List<AlumnoItem> _AlumnoItems = [];
  var isLoading = false;
  //String? _error;

  @override
  void initState() {
    super.initState();
    _loadIteams();
  }

  void _loadIteams() async {
    final url = Uri.https(
        'coperativa-flutter-default-rtdb.firebaseio.com', 'alumnos-list.json');
    final response = await http.get(url);
    final Map<String, dynamic> listData = json.decode(response.body);
    final List<AlumnoItem> loadedItems = [];

    for (final item in listData.entries) {
      final grado = grados.entries
          .firstWhere((element) => element.value.grado == item.value['grado'])
          .value;
      loadedItems.add(
        AlumnoItem(
          id: item.key,
          nombre: item.value['nombre'],
          aPaterno: item.value['apaterno'],
          aMaterno: item.value['amaterno'],
          anio: grado,
        ),
      );
    }
    setState(() {
      _AlumnoItems = loadedItems;
      isLoading = false;
    });
  }

  void _addItem() async {
    final newItem = await Navigator.of(context).push<AlumnoItem>(
      MaterialPageRoute(
        builder: (context) => const NewAlumno(),
      ),
    );
    if (newItem == null) {
      return;
    }
    setState(() {
      _AlumnoItems.add(newItem);
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget content = const Center(
      child: Text('Sin informaciòn'),
    );
    if (isLoading) {
      content = const Center(
        child: CircularProgressIndicator(),
      );
    }
    if (_AlumnoItems.isNotEmpty) {
      content = ListView.builder(
        itemCount: _AlumnoItems.length,
        itemBuilder: (ctx, index) => Dismissible(
          onDismissed: (direction) {
            // _removeItem(_groceryItems[index]);
          },
          key: ValueKey(_AlumnoItems[index].id),
          child: ListTile(
            title: Text(_AlumnoItems[index].nombre),
            leading: Container(
              width: 24,
              height: 24,
              color: Colors.white,
            ),
          ),
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lista de alumnos'),
        actions: [
          IconButton(
            onPressed: _addItem,
            icon: const Icon(Icons.add),
            color: Theme.of(context).colorScheme.primary,
          ),
          IconButton(
            onPressed: () {
              FirebaseAuth.instance.signOut();
            },
            icon: Icon(
              Icons.exit_to_app,
              color: Theme.of(context).colorScheme.primary,
            ),
          )
        ],
      ),
      body: content,
    );
  }
}
