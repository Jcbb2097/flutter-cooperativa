enum Perfiles { admin, registrar }

class Perfil {
  const Perfil(this.id, this.perfil);

  final String id;
  final String perfil;
}
