class ProductItem {
  final String id;
  final String nombre;
  final String color;
  final double precio;
  final int stock;

  const ProductItem({
    required this.id,
    required this.nombre,
    required this.color,
    required this.precio,
    required this.stock,
  });
}
