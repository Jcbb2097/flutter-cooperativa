import 'package:coperativa_app_2/models/category.dart';

class GroceryItem {
  final String id;
  final String name;
  final int quantity;
  final Category category;
  final int precio;

  const GroceryItem({
    required this.id,
    required this.name,
    required this.quantity,
    required this.category,
    required this.precio,
  });
}
