enum Perfil {
  administrativo,
  lectura,
}

class UsuarioItem {
  final String id;
  final String correo;
  final String usuario;
  final String paswoord;
  final String perfil;

  const UsuarioItem({
    required this.id,
    required this.correo,
    required this.usuario,
    required this.paswoord,
    required this.perfil,
  });
}
