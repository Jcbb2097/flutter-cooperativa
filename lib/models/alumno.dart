import 'package:coperativa_app_2/models/grado.dart';

class AlumnoItem {
  final String id;
  final String nombre;
  final String aPaterno;
  final String aMaterno;
  final Grado anio;

  const AlumnoItem({
    required this.id,
    required this.nombre,
    required this.aPaterno,
    required this.aMaterno,
    required this.anio,
  });
}
