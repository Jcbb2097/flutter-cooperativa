import 'package:flutter/material.dart';

enum Grupos {
  primero,
  segundo,
  tercero,
  cuarto,
  quinto,
  sexto,
}

class Grado {
  final String id;
  final String grado;
  final Color color;

  const Grado(this.id, this.grado, this.color);
}
