import 'package:flutter/material.dart';

enum Categories {
  cacahuates,
  palomitas,
  ceral,
  papas,
  dulces,
  helado,
  agua,
  colacion,
  desayuno,
  galletas,
  gomitas
}

class Category {
  const Category(this.title, this.color);

  final String title;
  final Color color;
}
