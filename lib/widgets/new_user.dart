import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import '../data/perfiles.dart';
import '../models/permisos.dart';
import '../models/users.dart';

class NewUser extends StatefulWidget {
  const NewUser({super.key});

  @override
  State<NewUser> createState() {
    return _NewUserState();
  }
}

class _NewUserState extends State<NewUser> {
  final _formKey = GlobalKey<FormState>();
  //var _isUploading = false;
  var _passwoordField = '';
  var _userField = '';
  var _enteredUsername = '';
  var _selectedCategory = perfiles[Perfiles.registrar]!;

  var isSending = false;

  void _saveItem() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      setState(() {
        isSending = true;
      });
      final url = Uri.https('coperativa-flutter-default-rtdb.firebaseio.com',
          'usarios-list.json');
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode(
          {
            'email': _userField,
            'usuario': _enteredUsername,
            'contraseña': _passwoordField,
            'perfil': _selectedCategory.id,
          },
        ),
      );

      final Map<String, dynamic> resData = json.decode(response.body);
      if (!context.mounted) {
        return;
      }
      Navigator.of(context).pop(UsuarioItem(
          id: resData['name'],
          correo: _userField,
          usuario: _enteredUsername,
          paswoord: _passwoordField,
          perfil: _selectedCategory.id));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Añade un nuevo usuario'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                decoration: const InputDecoration(labelText: 'Email Address'),
                keyboardType: TextInputType.emailAddress,
                autocorrect: false,
                textCapitalization: TextCapitalization.none,
                validator: (value) {
                  if (value == null ||
                      value.trim().isEmpty ||
                      !value.contains('@')) {
                    return 'Please enter a valid email address';
                  }
                  return null;
                },
                onSaved: (newValue) {
                  _userField = newValue!;
                },
              ),
              TextFormField(
                decoration: const InputDecoration(labelText: 'Username'),
                enableSuggestions: false,
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      value.trim().length < 4) {
                    return 'please enter a valid username';
                  }
                  return null;
                },
                onSaved: (newValue) {
                  _enteredUsername = newValue!;
                },
              ),
              TextFormField(
                decoration: const InputDecoration(labelText: 'Password'),
                obscureText: true,
                validator: (value) {
                  if (value == null || value.trim().length < 6) {
                    return 'Password must be at least 6 characters long';
                  }
                  return null;
                },
                onSaved: (newValue) {
                  _passwoordField = newValue!;
                },
              ),
              DropdownButtonFormField(
                value: _selectedCategory,
                items: [
                  for (final category in perfiles.entries)
                    DropdownMenuItem(
                      value: category.value,
                      child: Row(children: [
                        Container(
                          width: 16,
                          height: 16,
                          color: Colors.blueGrey,
                        ),
                        const SizedBox(width: 6),
                        Text(category.value.perfil),
                      ]),
                    )
                ],
                onChanged: (value) {
                  setState(() {
                    _selectedCategory = value!;
                  });
                },
              ),
              const SizedBox(height: 12),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: isSending
                        ? null
                        : () {
                            _formKey.currentState!.reset();
                          },
                    child: const Text('reset'),
                  ),
                  ElevatedButton(
                    onPressed: isSending ? null : _saveItem,
                    child: isSending
                        ? const SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator(),
                          )
                        : const Text('agregar item'),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
