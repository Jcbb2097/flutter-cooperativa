import 'package:flutter/material.dart';

class ColorUserPiket extends StatefulWidget {
  const ColorUserPiket({
    super.key,
    required this.onPickColor,
  });

  final void Function(Color pickedColor) onPickColor;

  @override
  State<ColorUserPiket> createState() {
    return _ColorUserPiketState();
  }
}

class _ColorUserPiketState extends State<ColorUserPiket> {
  Color pickerColor = Color(0xff443a49);
  Color currentColor = Color(0xff443a49);

// ValueChanged<Color> callback
  void changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
