import 'dart:convert';

import 'package:coperativa_app_2/data/categories.dart';
import 'package:coperativa_app_2/models/grocery_item.dart';
import 'package:coperativa_app_2/screens/alumnos.dart';
import 'package:coperativa_app_2/screens/productos.dart';
import 'package:coperativa_app_2/screens/usuarios.dart';
import 'package:coperativa_app_2/widgets/main_drawer.dart';
import 'package:coperativa_app_2/widgets/new_iteam.dart';
import 'package:coperativa_app_2/widgets/pagar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

final _firebase = FirebaseAuth.instance;

class GroceryList extends StatefulWidget {
  const GroceryList({super.key});

  @override
  State<GroceryList> createState() => _GroceryListState();
}

class _GroceryListState extends State<GroceryList> {
  List<GroceryItem> _groceryItems = [];
  var isLoading = true;
  var precio = 0;
  //String? _error;

  @override
  void initState() {
    super.initState();
    _loadIteams();
  }

  void _setScreen(String identifier) {
    Navigator.of(context).pop();
    switch (identifier) {
      case 'usuarios':
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => const UsuariosLista(),
        ));
        break;
      case 'productos':
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => const Productoslista(),
        ));
        break;
      case 'alumnos':
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => const ListaAlumnos(),
        ));
        break;
      case 'list':
        break;
      default:
        break;
    }
  }

  void _loadIteams() async {
    final url = Uri.https('coperativa-flutter-default-rtdb.firebaseio.com',
        'cooperativa-list.json');
    final response = await http.get(url);
    final Map<String, dynamic> listData = json.decode(response.body);

    final List<GroceryItem> loadedItems = [];
    for (final item in listData.entries) {
      final category = categories.entries
          .firstWhere(
              (element) => element.value.title == item.value['categoria'])
          .value;
      loadedItems.add(
        GroceryItem(
          id: item.key,
          name: item.value['nombre'],
          quantity: item.value['cantidad'],
          category: category,
          precio: item.value['precio'],
        ),
      );
    }

    setState(() {
      _groceryItems = loadedItems;
      isLoading = false;
    });
  }

  void _obtenerCuenta(producto) {}

  void _addItem() async {
    final newItem = await Navigator.of(context).push<GroceryItem>(
      MaterialPageRoute(
        builder: (context) => const NewItem(),
      ),
    );
    if (newItem == null) {
      return;
    }
    setState(() {
      _groceryItems.add(newItem);
    });
  }

  _payAcount() async {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => const Pagar(
              id: 'sdsdsd',
            )));
  }

  @override
  Widget build(BuildContext context) {
    Widget content = const Center(
      child: Text('Sin informaciòn'),
    );
    if (isLoading) {
      content = const Center(
        child: CircularProgressIndicator(),
      );
    }
    if (_groceryItems.isNotEmpty) {
      content = ListView.builder(
        itemCount: _groceryItems.length,
        itemBuilder: (ctx, index) => Dismissible(
          onDismissed: (direction) {
            // _removeItem(_groceryItems[index]);
          },
          key: ValueKey(_groceryItems[index].id),
          child: ListTile(
            title: Text(_groceryItems[index].category.title),
            leading: Text(_groceryItems[index].name),
            trailing: TextButton(
              onPressed: _payAcount,
              child: const Icon(Icons.monetization_on_outlined),
            ),
          ),
        ),
      );
    }
    // if (_groceryItems.isNotEmpty) {
    //   content = ListView.builder(
    //    itemCount: _groceryItems.length,
    // itemBuilder: (ctx, index) => Dismissible(
    //    onDismissed: (direction) {
    // _removeItem(_groceryItems[index]);
    //   },
    //   key: ValueKey(_groceryItems[index].id),
    //  child: ListTile(
    //   title: Text(_groceryItems[index].name),
    //  leading: Container(
    //    width: 24,
    //   height: 24,
    //  color: _groceryItems[index].category.color,
    //  ),
    // trailing: Text(
    // _groceryItems[index].quantity.toString(),
    // ),
    // ),
    //  ),
    // );
    //  }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Cooperativa'),
        actions: [
          IconButton(
            onPressed: _addItem,
            icon: const Icon(Icons.add),
            color: Theme.of(context).colorScheme.primary,
          ),
          IconButton(
            onPressed: () {
              FirebaseAuth.instance.signOut();
            },
            icon: Icon(
              Icons.exit_to_app,
              color: Theme.of(context).colorScheme.primary,
            ),
          )
        ],
      ),
      drawer: MainDrawer(
        onSelectScreen: _setScreen,
      ),
      body: content,
    );
  }
}
