import 'package:flutter/material.dart';

class Pagar extends StatefulWidget {
  const Pagar({super.key, required this.id});
  final String id;

  @override
  State<Pagar> createState() {
    return _PagarState();
  }
}

// ignore: camel_case_types
class _PagarState extends State<Pagar> {
  final _formKey = GlobalKey<FormState>();
  var _priceProduct = 0;
  @override
  void _pagar() {
    Navigator.of(context).pop();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pagar cuenta'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Card(
                margin: const EdgeInsets.all(20),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Form(
                        child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                          decoration:
                              const InputDecoration(labelText: 'Usuario'),
                          initialValue: 'Carlos',
                        ),
                        TextFormField(
                          decoration:
                              const InputDecoration(labelText: 'Producto'),
                          initialValue: 'cacahuates',
                        ),
                        TextFormField(
                          decoration: const InputDecoration(labelText: 'Pagar'),
                          initialValue: '50 pesos',
                        ),
                        TextFormField(
                            decoration: const InputDecoration(
                              label: Text('Precio del producto'),
                            ),
                            initialValue: _priceProduct.toString(),
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value == null ||
                                  value.isEmpty ||
                                  int.tryParse(value) == null ||
                                  int.tryParse(value)! <= 0) {
                                return 'Debe de ser un numero valido';
                              }
                              return null;
                            },
                            onSaved: (newValue) {
                              _priceProduct = int.parse(newValue!);
                            }),
                        const SizedBox(height: 12),
                        ElevatedButton(
                          onPressed: _pagar,
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Theme.of(context)
                                  .colorScheme
                                  .primaryContainer),
                          child: const Text('pagar'),
                        ),
                      ],
                    )),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
