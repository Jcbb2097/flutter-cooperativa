import 'dart:convert';

import 'package:coperativa_app_2/data/grados.dart';
import 'package:coperativa_app_2/models/alumno.dart';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import '../models/grado.dart';

class NewAlumno extends StatefulWidget {
  const NewAlumno({super.key});

  @override
  State<NewAlumno> createState() {
    return _NewAlumnoState();
  }
}

class _NewAlumnoState extends State<NewAlumno> {
  final _formKey = GlobalKey<FormState>();
  var _nombreAlumno = '';
  var _aMaterno = '';
  var _aPaterno = '';
  var _selectedGrado = grados[Grupos.primero]!;
  var isSending = false;

  void _saveItem() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      final url = Uri.https('coperativa-flutter-default-rtdb.firebaseio.com',
          'alumnos-list.json');
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode(
          {
            'nombre': _nombreAlumno,
            'apaterno': _aPaterno,
            'amaterno': _aMaterno,
            'grado': _selectedGrado.grado,
          },
        ),
      );
      //print(response.body);
      final Map<String, dynamic> resData = json.decode(response.body);
      if (!context.mounted) {
        return;
      }
      Navigator.of(context).pop(
        AlumnoItem(
          id: resData['name'],
          nombre: _nombreAlumno,
          aPaterno: _aPaterno,
          aMaterno: _aMaterno,
          anio: _selectedGrado,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Añade un nuevo Alumno'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                maxLength: 50,
                decoration: const InputDecoration(
                  label: Text('Nombre'),
                ),
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      value.trim().length <= 1 ||
                      value.trim().length > 50) {
                    return 'Debe de contener mas de 1 y menos de 50';
                  }
                  return null;
                },
                onSaved: (newValue) {
                  _nombreAlumno = newValue!;
                },
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: TextFormField(
                      maxLength: 50,
                      decoration: const InputDecoration(
                        label: Text('Apellido Paterno'),
                      ),
                      validator: (value) {
                        if (value == null ||
                            value.isEmpty ||
                            value.trim().length <= 1 ||
                            value.trim().length > 50) {
                          return 'Debe de contener mas de 1 y menos de 50';
                        }
                        return null;
                      },
                      onSaved: (newValue) {
                        _aPaterno = newValue!;
                      },
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: TextFormField(
                      maxLength: 50,
                      decoration: const InputDecoration(
                        label: Text('Apellido Materno'),
                      ),
                      validator: (value) {
                        if (value == null ||
                            value.isEmpty ||
                            value.trim().length <= 1 ||
                            value.trim().length > 50) {
                          return 'Debe de contener mas de 1 y menos de 50';
                        }
                        return null;
                      },
                      onSaved: (newValue) {
                        _aMaterno = newValue!;
                      },
                    ),
                  ),
                ],
              ),
              DropdownButtonFormField(
                value: _selectedGrado,
                items: [
                  for (final grado in grados.entries)
                    DropdownMenuItem(
                      value: grado.value,
                      child: Row(children: [
                        Container(
                          width: 16,
                          height: 16,
                          color: grado.value.color,
                        ),
                        const SizedBox(width: 6),
                        Text(grado.value.grado),
                      ]),
                    )
                ],
                onChanged: (value) {
                  setState(() {
                    _selectedGrado = value!;
                  });
                },
              ),
              const SizedBox(height: 12),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: isSending
                        ? null
                        : () {
                            _formKey.currentState!.reset();
                          },
                    child: const Text('reset'),
                  ),
                  ElevatedButton(
                    onPressed: isSending ? null : _saveItem,
                    child: isSending
                        ? const SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator(),
                          )
                        : const Text('agregar item'),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
