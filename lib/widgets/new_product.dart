import 'dart:convert';

import 'package:coperativa_app_2/models/product.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class NewProducto extends StatefulWidget {
  const NewProducto({super.key});

  @override
  State<NewProducto> createState() {
    return _NewProductoState();
  }
}

class _NewProductoState extends State<NewProducto> {
  final _formKey = GlobalKey<FormState>();
  var _nombreProduct = '';
  var _colorPickectProduct = '';
  double _priceProduct = 1.00;
  var _stockProduct = 1;

  var isSending = false;

  void _saveItem() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      final url = Uri.https('coperativa-flutter-default-rtdb.firebaseio.com',
          'productos-list.json');
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode(
          {
            'nombre': _nombreProduct,
            'precio': _priceProduct,
            'stock': _stockProduct,
            'color': _colorPickectProduct,
          },
        ),
      );
      //print(response.body);
      final Map<String, dynamic> resData = json.decode(response.body);
      if (!context.mounted) {
        return;
      }
      Navigator.of(context).pop(ProductItem(
          id: resData['name'],
          nombre: _nombreProduct,
          color: _colorPickectProduct,
          precio: _priceProduct,
          stock: _stockProduct));
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: const Text('Añade un nuevo producto'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                maxLength: 50,
                decoration: const InputDecoration(
                  label: Text('nombre del producto'),
                ),
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      value.trim().length <= 1 ||
                      value.trim().length > 50) {
                    return 'Debe de contener mas de 1 y menos de 50';
                  }
                  return null;
                },
                onSaved: (newValue) {
                  _nombreProduct = newValue!;
                },
              ),
              TextFormField(
                maxLength: 8,
                decoration: const InputDecoration(
                  label: Text('Color del producto'),
                ),
                validator: (value) {
                  if (value == null ||
                      value.isEmpty ||
                      value.trim().length <= 1 ||
                      value.trim().length > 8) {
                    return 'Debe de contener mas de 1 y menos de 8';
                  }
                  return null;
                },
                onSaved: (newValue) {
                  _colorPickectProduct = newValue!;
                },
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: TextFormField(
                        decoration: const InputDecoration(
                          label: Text('Precio del producto'),
                        ),
                        initialValue: _priceProduct.toString(),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              int.tryParse(value) == null ||
                              int.tryParse(value)! <= 0) {
                            return 'Debe de ser un numero valido';
                          }
                          return null;
                        },
                        onSaved: (newValue) {
                          _priceProduct = double.parse(newValue!);
                        }),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: TextFormField(
                        decoration: const InputDecoration(
                          label: Text('Stock del producto'),
                        ),
                        initialValue: _stockProduct.toString(),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              int.tryParse(value) == null ||
                              int.tryParse(value)! <= 0) {
                            return 'Debe de ser un numero valido';
                          }
                          return null;
                        },
                        onSaved: (newValue) {
                          _stockProduct = int.parse(newValue!);
                        }),
                  ),
                ],
              ),
              const SizedBox(height: 12),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: isSending
                        ? null
                        : () {
                            _formKey.currentState!.reset();
                          },
                    child: const Text('reset'),
                  ),
                  ElevatedButton(
                    onPressed: isSending ? null : _saveItem,
                    child: isSending
                        ? const SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator(),
                          )
                        : const Text('agregar item'),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
