import '../models/permisos.dart';

const perfiles = {
  Perfiles.admin: Perfil('1', 'Administrar'),
  Perfiles.registrar: Perfil('2', 'Registrar'),
};
