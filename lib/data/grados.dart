import 'package:flutter/material.dart';

import '../models/grado.dart';

const grados = {
  Grupos.primero: Grado('1', 'Primero', Colors.pinkAccent),
  Grupos.segundo: Grado('2', 'Segundo', Colors.yellow),
  Grupos.tercero: Grado('3', 'Tercero', Colors.orange),
  Grupos.cuarto: Grado('4', 'Cuarto', Colors.blueAccent),
  Grupos.quinto: Grado('5', 'Quinto', Colors.red),
  Grupos.sexto: Grado('6', 'Sexto', Colors.green),
};
