import 'package:flutter/material.dart';

import 'package:coperativa_app_2/models/category.dart';

const categories = {
  Categories.agua: Category(
    'agua',
    Color.fromARGB(255, 5, 120, 235),
  ),
  Categories.cacahuates: Category(
    'cacahuates',
    Color.fromARGB(201, 101, 56, 13),
  ),
  Categories.ceral: Category(
    'ceral',
    Color.fromARGB(255, 255, 102, 0),
  ),
  Categories.colacion: Category(
    'colacion',
    Color.fromARGB(255, 60, 255, 0),
  ),
  Categories.desayuno: Category(
    'desayuno',
    Color.fromARGB(255, 205, 234, 17),
  ),
  Categories.dulces: Category(
    'dulces',
    Color.fromARGB(255, 255, 0, 183),
  ),
  Categories.galletas: Category(
    'galletas',
    Color.fromARGB(164, 200, 150, 13),
  ),
  Categories.gomitas: Category(
    'gomitas',
    Color.fromARGB(255, 10, 127, 150),
  ),
  Categories.helado: Category(
    'helado',
    Color.fromARGB(255, 99, 79, 79),
  ),
  Categories.palomitas: Category(
    'palomitas',
    Color.fromARGB(255, 102, 28, 28),
  ),
  Categories.papas: Category(
    'palomitas',
    Color.fromARGB(255, 134, 255, 21),
  ),
};
